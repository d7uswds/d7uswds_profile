<?php
/**
 * @file
 * d7uswds_setup.bean.inc
 */

/**
 * Implements hook_bean_admin_ui_types().
 */
function d7uswds_setup_bean_admin_ui_types() {
  $export = array();

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'emergency_message';
  $bean_type->label = 'Emergency message';
  $bean_type->options = '';
  $bean_type->description = '';
  $export['emergency_message'] = $bean_type;

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'footer_secondary_section';
  $bean_type->label = 'Footer Secondary Section';
  $bean_type->options = '';
  $bean_type->description = 'usa-grid with footer logo and social links list.';
  $export['footer_secondary_section'] = $bean_type;

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'general_content';
  $bean_type->label = 'General content';
  $bean_type->options = '';
  $bean_type->description = '';
  $export['general_content'] = $bean_type;

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'header_dropdown_banner';
  $bean_type->label = 'Header Dropdown Banner';
  $bean_type->options = '';
  $bean_type->description = 'Collapsible banner to display in Header.';
  $export['header_dropdown_banner'] = $bean_type;

  return $export;
}

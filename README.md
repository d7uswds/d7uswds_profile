d7uswds Profile
===========

d7uswds Drupal installation profile

Documentation:


d7uswds Profile
===========

d7uswds - a Drupal installation profile based on US Web Design Standards (v1.0).


Features:
------

* Removal of unnecessary default Drupal modules & settings (e.g. overlay etc)
* Removal of default content types, leaving ‘page’.
* Text formats / WYSIWYG setup & config
* Editor, Super Editor & Administrator roles setup & given basic permissions
* Default node path structure setup
* jQuery_update enabled by default, using jQuery 1.8 front end & 1.5 backend
* Breakpoints/picture module installed & setup by default
* Basic user account settings configured
* Time/date settings configured
* XML sitemap installed & configured
* Backup & migrate schedule configured to backup once daily and keep 7 days of backups
* Default Drupal admin paths changed for security (from /user to /member/login)
* Default admin theme setup & configured (Adminimal)
* Unnecessary Drupal blocks removed
* Common contrib modules installed
* Emergency message setup. Using ‘Bean’ module, super-editors can add a global emergency message to the header of the site (found at /block/emergency-message/edit)
* Node field labels are set to ‘hidden’ by default

Included modules:
------
\# | #
:---:|:---:
| admin_menu	| menu_block
| admin_views	| menu_position
| administerusersbyrole	| metatag
| backup_migrate	| multiupload_imagefield_widget
| bean	| multiupload_filefield_widget
| better_exposed_filters	| module_filter
| breakpoints	| panels
| chain_menu_access	| pathauto
| ckeditor	| permission_select
| ckeditor_link	| picture
| ctools	| publishcontent
| date	| redirect
| devel	| rename_admin_paths
| eck	| Role_delegation
| entity	| schemaorg
| entityreference	| semantic_panels (1.x-dev)
| features	| site_map
| field_formatter_settings	| smart_trim
| field_group	| stage_file_proxy
| field_tools	| strongarm
| google_analytics	| d7uswds_profile
| hide_formats	| d7uswds_tweaks
| image	| token
| inline_entity_form	| view_unpublished
| jquery_update	| views
| libraries	| views_bulk_operations
| link	| xmlsitemap
| menu_admin_per_menu	| menu_attributes
|


Included themes:
----
* [Adminimal theme](https://www.drupal.org/project/adminimal_theme)
* Drubath (Drupal Base Theme)
* wdsd (Theme based on US Web design Standards v1.0)

Included libraries:
----
* ckeditor

Module notes:
----
* Bean

  This module is used for homeless content (e.g. header/footer copy) that wants to be content manageable by the client without having to give them access to core Blocks (more faff than is healthy)

* Multiupload image/filefield widget

  These modules enable the user to upload multiple files/images in a single go. Useful for gallery fields/bulk download fields and means the user doesn’t have to just upload a single file at a time

* Stage file proxy

  Super useful module that should only be enabled/used on your local environment. Once installed go to the configuration page and enter the live/dev site url. It will then load images & files from that environment so you don’t have to manually sync the ‘files’ folder to work on local with full images etc



<?php
/**
 * @file
 * d7uswds_node_samples.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function d7uswds_node_samples_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

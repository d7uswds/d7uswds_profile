<?php
/**
 * @file
 * d7uswds_setup.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function d7uswds_setup_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "backup_migrate" && $api == "backup_migrate_exportables") {
    return array("version" => "1");
  }
  if ($module == "bean_admin_ui" && $api == "bean") {
    return array("version" => "5");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function d7uswds_setup_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function d7uswds_setup_image_default_styles() {
  $styles = array();

  // Exported image style: icons.
  $styles['icons'] = array(
    'label' => 'icons',
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 57,
          'height' => 57,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: media_block.
  $styles['media_block'] = array(
    'label' => 'Media Block(124x124)',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 124,
          'height' => 124,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function d7uswds_setup_node_info() {
  $items = array(
    'documentation' => array(
      'name' => t('Documentation'),
      'base' => 'node_content',
      'description' => t('If you’re presenting detailed information on a specific topic or theme that has already been contextualized by a landing page. Some topics that can be nicely represented on this type of page include guides or how-tos, technical documentation, and program descriptions — in short, any subject that requires in-depth explanation.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'landing' => array(
      'name' => t('Landing'),
      'base' => 'node_content',
      'description' => t('We use this CT for Landing Pages.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function d7uswds_setup_paragraphs_info() {
  $items = array(
    'graphic_list' => array(
      'name' => 'Graphic List',
      'bundle' => 'graphic_list',
      'locked' => '1',
    ),
    'header_banner' => array(
      'name' => 'Header Banner',
      'bundle' => 'header_banner',
      'locked' => '1',
    ),
    'hero_section' => array(
      'name' => 'Hero Section',
      'bundle' => 'hero_section',
      'locked' => '1',
    ),
    'section' => array(
      'name' => 'Text Section',
      'bundle' => 'section',
      'locked' => '1',
    ),
    'tagline_section' => array(
      'name' => 'Tagline Section',
      'bundle' => 'tagline_section',
      'locked' => '1',
    ),
  );
  return $items;
}

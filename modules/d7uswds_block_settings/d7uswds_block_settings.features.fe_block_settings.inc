<?php
/**
 * @file
 * d7uswds_block_settings.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function d7uswds_block_settings_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['bean-secondary-footer'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'secondary-footer',
    'module' => 'bean',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'wdsd' => array(
        'region' => 'secondfooter',
        'status' => 1,
        'theme' => 'wdsd',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['search-form'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'form',
    'module' => 'search',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'wdsd' => array(
        'region' => 'search',
        'status' => 1,
        'theme' => 'wdsd',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['simple_toc-simple_toc'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'simple_toc',
    'module' => 'simple_toc',
    'node_types' => array(
      0 => 'documentation',
      1 => 'page',
    ),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'wdsd' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'wdsd',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  return $export;
}

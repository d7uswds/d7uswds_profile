<?php
/**
 * @file
 * d7uswds_node_samples.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function d7uswds_node_samples_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'title' => 'Docs title',
  'log' => '',
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'type' => 'documentation',
  'language' => 'und',
  'created' => 1488903161,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => 'a6574af8-a342-48da-90bc-4b84001a2409',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => '<h2>Section heading (h2)</h2>

<p>These headings introduce, respectively, sections and subsections within your body copy. As you create these headings, follow the same guidelines that you use when writing section headings: Be succinct, descriptive, and precise.</p>

<h3>Subsection heading (h3)</h3>

<p>The particulars of your body copy will be determined by the topic of your page. Regardless of topic, it’s a good practice to follow the inverted pyramid structure when writing copy: Begin with the information that’s most important to your users and then present information of less importance.</p>

<p>Keep each section and subsection focused — a good approach is to include one theme (topic) per section.</p>

<h4>Subsection heading (h4)</h4>

<p>Use the side navigation menu to help your users quickly skip to different sections of your page. The menu is best suited to displaying a hierarchy with one to three levels and, as we mentioned, to display the sub-navigation of a given page.</p>

<p>Read the full documentation on our side navigation on the component page.</p>
',
        'summary' => '',
        'format' => 'full_html',
        'safe_summary' => '',
      ),
    ),
  ),
  'field_highlight' => array(
    'und' => array(
      0 => array(
        'value' => 'The page heading communicates the main focus of the page. Make your page heading descriptive and keep it succinct.',
        'format' => NULL,
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'foaf:Document',
      1 => 'sioc:Item',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'field_highlight' => array(
      'predicates' => array(),
    ),
  ),
  'path' => array(
    'pathauto' => 1,
  ),
  'simple_toc' => array(
    'value' => '<ul class="usa-sidenav-list"><li class="current"><a href="javascript:void(0);" class="usa-current">Docs title</a></li><ul class="usa-sidenav-sub_list"><li class="toc-h2"><a href="#section-heading-1" class="toc-h2">Section heading (h2)</a></li><li class="toc-h3"><a href="#section-heading-2" class="toc-h3">Subsection heading (h3)</a></li><li class="toc-h4"><a href="#section-heading-3" class="toc-h4">Subsection heading (h4)</a></li></ul></ul>
',
    'title' => 'Docs title',
    'access' => FALSE,
  ),
  'pathauto_perform_alias' => FALSE,
  'date' => '2017-03-07 11:12:41 -0500',
  'user_uuid' => '3c057f56-c96e-4ecf-816a-05368fa0bbc8',
);
  return $nodes;
}
